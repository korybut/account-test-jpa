package com.korybut.accounttestjpa.repository;

import com.korybut.accounttestjpa.model.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends CrudRepository<Account, Integer> {

}
