package com.korybut.accounttestjpa.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="accounts_test_db")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account {

    @Id
    private int id;
    private String nick;
    private String password;
    private int age;
    private String state;

}
