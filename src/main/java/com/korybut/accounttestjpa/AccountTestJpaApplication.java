package com.korybut.accounttestjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccountTestJpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(AccountTestJpaApplication.class, args);
    }
}
