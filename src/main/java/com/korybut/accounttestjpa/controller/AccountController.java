package com.korybut.accounttestjpa.controller;

import com.korybut.accounttestjpa.model.Account;
import com.korybut.accounttestjpa.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class AccountController {

    @Autowired
    private AccountRepository repository;

    @PostMapping("/saveAccounts")
    public String saveAccounts(@RequestBody List<Account> accounts) {
        repository.saveAll(accounts);
        return accounts.size() + " accounts saved...";
    }

    @GetMapping("/getAllAccounts")
    public List<Account> getAllAccounts() {
        return (List<Account>) repository.findAll();
    }

    @GetMapping("/getById/{id}")
    public Optional<Account> getById(@PathVariable int id) {
        return repository.findById(id);
    }

}
